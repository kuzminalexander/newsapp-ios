//
//  NewsViewModel.swift
//  NewsApp
//
//  Created by Александр on 13/04/2019.
//

import Foundation
import UIKit
import CoreData

public enum LoadState{
    case loading
    case free
}

class NewsViewModel{
    
    weak var dataSource : GenericDataSource<Article>?
    weak var service: NewsServiceProtocol?
    var tempArticles : [Article]?
    var inSearchProcessing : Bool = false
    var currentPage = 1
    public var state : LoadState = .free
    let pageSize = 20
    var allIsLoad = false
    
    var onErrorHandling : ((ErrorResult?) -> Void)?
    
    init(service: NewsServiceProtocol = NewsService.shared, dataSource : GenericDataSource<Article>?) {
        self.dataSource = dataSource
        self.service = service
    }
    
    func setupCachedArticles(){
        let context = CoreDataService.shared.persistentContainer.viewContext
        do{
            let fetchRequest = NSFetchRequest<Article>(entityName: "Article")
            let sort = NSSortDescriptor(key: #keyPath(Article.order), ascending: true)
            fetchRequest.sortDescriptors = [sort]
            self.dataSource?.data.value = try context.fetch(fetchRequest)
            self.tempArticles = self.dataSource?.data.value
        } catch let error{
            print("\(error)")
        }
    }
    
    func loadMoreArticles(fromFirstPage:Bool){
        if (self.state == .loading){
            return
        }
        self.state = .loading
        guard let service = service else {
            self.state = .free
            onErrorHandling?(ErrorResult.custom(string: "Missing service"))
            return
        }
        
        if fromFirstPage{
            currentPage = 1
            allIsLoad = false
        }
        
        service.fetchNews(page: self.currentPage, pageSize: self.pageSize){ result in
            DispatchQueue.main.async {
                switch result {
                case .success(let news) :
                    if let articles = self.dataSource?.data.value{
                        if(self.currentPage==1){
                            CoreDataService.shared.removeArticles(articlesForRemove: articles)
                        }
                    }
                    if(news.articles.count<=0){
                        self.allIsLoad = true
                        self.state = .free
                        return
                    }
                    let context = CoreDataService.shared.persistentContainer.viewContext
                    if (self.currentPage==1){
                        self.dataSource?.data.value = news.articles
                    }else{
                        self.dataSource?.data.value.append(contentsOf: news.articles)
                    }
                    self.tempArticles = self.dataSource?.data.value
                    self.setArticleOrder()
                    do{
                        try context.save()
                    } catch let error {
                        print("Could not save \(error)")
                    }
                    self.currentPage+=1
                case .failure(let error) :
                    self.onErrorHandling?(error)
                }
                self.state = .free
            }
        }
    }
    
    func searchWithText(searchText:String){
        if(searchText.isEmpty){
            inSearchProcessing = false
            self.dataSource?.data.value = tempArticles ?? []
            return
        }
        inSearchProcessing = true
        self.dataSource?.data.value = tempArticles?.filter({ (article) -> Bool in
            return (article.title?.lowercased().contains(searchText.lowercased()) ?? false || article.author?.lowercased().contains(searchText.lowercased()) ?? false )
        }) ?? []
    }
    
    private func setArticleOrder(){
        if let articles = self.dataSource?.data.value{
            for (index, element) in articles.enumerated() {
                element.setValue(index, forKey: "order")
            }
        }
    }

}
