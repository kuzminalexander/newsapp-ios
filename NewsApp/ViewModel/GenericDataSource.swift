//
//  GenericDataSource.swift
//  NewsApp
//
//  Created by Александр on 13/04/2019.
//

import Foundation

class GenericDataSource<T> : NSObject {
    var data: DynamicValue<[T]> = DynamicValue([])
}
