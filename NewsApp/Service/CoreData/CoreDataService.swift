//
//  CoreDataService.swift
//  NewsApp
//
//  Created by Александр on 14/04/2019.
//

import Foundation
import CoreData

class CoreDataService{
    
    private let coreDataName : String
    static let shared = CoreDataService(coreDataName: "CoreDataModel")
    
    private init(coreDataName : String){
        self.coreDataName = coreDataName
    }

    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: self.coreDataName)
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func removeAllArticles(){
        do {
            let results = try self.persistentContainer.viewContext.fetch(Article.fetchRequest())
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                self.persistentContainer.viewContext.delete(objectData)
            }
        } catch let error {
            print("Detele all data in \(error) error")
        }
    }
    
    func removeArticles(articlesForRemove:[Article]){
        for object in articlesForRemove {
            self.persistentContainer.viewContext.delete(object)
        }
    }
    
}
