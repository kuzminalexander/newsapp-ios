//
//  Result.swift
//  TemplateProject
//
//  Created by Benoit PASQUIER on 13/01/2018.
//

import Foundation

enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}
