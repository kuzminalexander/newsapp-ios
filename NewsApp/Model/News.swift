//
//  News.swift
//  NewsApp
//
//  Created by Александр on 13/04/2019.
//

import Foundation
import CoreData
import UIKit

struct News
{
    let totalResult : Int
    let articles : [Article]
}

extension News : Parceable{
    
    static func parseObject(dictionary: [String : AnyObject]) -> Result<News, ErrorResult> {
        if let totalResult = dictionary["totalResults"] as? Int,
        let articles = dictionary["articles"] as? [[String:AnyObject]] {
            var articlesArray = [Article]()
            for articleDict in articles{
                let article = Article(entity: NSEntityDescription.entity(forEntityName: "Article", in: CoreDataService.shared.persistentContainer.viewContext)!, insertInto: CoreDataService.shared.persistentContainer.viewContext)
                if let title = articleDict["title"] as? String{
                    article.setValue(title, forKey: "title")
                } else {
                    article.setValue("", forKey: "title")
                }
                if let author = articleDict["author"] as? String{
                    article.setValue(author, forKey: "author")
                } else{
                    article.setValue("", forKey: "author")
                }
                if let urlToImage = articleDict["urlToImage"] as? String{
                    article.setValue(urlToImage, forKey: "urlToImage")
                } else {
                    article.setValue("", forKey: "urlToImage")
                }
                if let sourceName = articleDict["source"]?["name"] as? String{
                    article.setValue(sourceName, forKey: "sourceName")
                } else {
                    article.setValue("", forKey: "soureName")
                }
                if let body = articleDict["description"] as? String{
                    article.setValue(body, forKey: "body")
                } else{
                    article.setValue("", forKey: "body")
                }
                if let publishedAt = articleDict["publishedAt"] as? String{
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                    let date = dateFormatter.date(from: publishedAt)!
                    article.setValue(date, forKey: "publishedDate")
                }
                articlesArray.append(article)
            }
            let news = News(totalResult: totalResult, articles: articlesArray)
            return Result.success(news)
        } else {
            return Result.failure(ErrorResult.parser(string: "Unable to parse news"))
        }
    }
    
}
