//
//  Article+CoreDataProperties.swift
//  NewsApp
//
//  Created by Александр on 14/04/2019.
//
//

import Foundation
import CoreData


extension Article {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Article> {
        return NSFetchRequest<Article>(entityName: "Article")
    }

    @NSManaged public var author: String?
    @NSManaged public var title: String?
    @NSManaged public var sourceName: String?
    @NSManaged public var urlToImage: String?
    @NSManaged public var order: Int
    @NSManaged public var publishedDate : Date?
    @NSManaged public var body : String?

}
