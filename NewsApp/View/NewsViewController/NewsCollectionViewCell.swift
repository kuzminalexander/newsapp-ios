//
//  NewsCollectionViewCell.swift
//  NewsApp
//
//  Created by Александр on 13/04/2019.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var authorTitle: UILabel!
    @IBOutlet weak var dateTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var article : Article? {
        didSet {
            guard let article = article else {
                return
            }
            newsTitleLabel?.text = article.title
            newsImageView.image = article.loadedImage
            authorTitle?.text = article.author
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale.current
            dateFormatter.dateFormat = "dd-MM-yyyy"
            if let publishedDate = article.publishedDate{
                dateTitle?.text = dateFormatter.string(from: publishedDate)
            } else{
                dateTitle?.text = ""
            }
        }
    }
}
