//
//  NewsViewController.swift
//  NewsApp
//
//  Created by Александр on 13/04/2019.
//

import UIKit

class NewsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private let dataSource = NewsDataSource()
    private let refreshControl = UIRefreshControl()
    private var searchBar: UISearchBar?
    private var openSearchBarButton : UIBarButtonItem?
    private var titleView : UIView?
    private var barBackgroundImage : UIImage?

    lazy var viewModel : NewsViewModel = {
        let viewModel = NewsViewModel(dataSource: dataSource)
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Новости"
        
        openSearchBarButton = UIBarButtonItem(image: UIImage.init(named: "search_icon"), style: .plain, target: self, action: #selector(openSearchBarButtonClicked))
        self.navigationItem.rightBarButtonItem = openSearchBarButton
        self.barBackgroundImage = self.navigationController?.navigationBar.backgroundImage(for: .default)
        
        searchBar = UISearchBar()
        searchBar?.placeholder = "Поиск"
        searchBar?.showsCancelButton = true
        searchBar?.sizeToFit()
        searchBar?.delegate = self;
        
        titleView = navigationItem.titleView
        
        self.collectionView.register(UINib(nibName: "NewsCollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "newsCell")

        self.collectionView.register(LoadFooterView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "loadFooterId")
        (self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).footerReferenceSize = CGSize(width: collectionView.bounds.width, height: 50)
        
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self.dataSource
        self.dataSource.data.addAndNotify(observer: self) { [weak self] _ in
            self?.hideRefreshIfRunning()
            self?.collectionView.reloadData()
            print("customLog: collection view reload")
        }
        
        self.viewModel.setupCachedArticles()
        
        if #available(iOS 10.0, *) {
            self.collectionView.refreshControl = refreshControl
        } else {
            self.collectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        
        self.viewModel.onErrorHandling = { [weak self] error in
            self?.hideRefreshIfRunning()
            let controller = UIAlertController(title: "Ошибка", message: "Что-то пошло не так", preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: nil))
            self?.present(controller, animated: true, completion: nil)
        }
        
        self.viewModel.loadMoreArticles(fromFirstPage: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(self.barBackgroundImage, for: .default)
    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
        self.viewModel.loadMoreArticles(fromFirstPage: true)
    }
    
    private func hideRefreshIfRunning(){
        if self.refreshControl.isRefreshing{
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc private func openSearchBarButtonClicked(){
        UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions(), animations: {
            [weak self] in
                self?.collectionView.setContentOffset(CGPoint(x:0,y:0), animated: true)
                self?.navigationItem.rightBarButtonItem = nil;
                self?.navigationItem.titleView = self?.searchBar;
                self?.searchBar?.becomeFirstResponder()
        }, completion: nil)
    }
    
}

extension NewsViewController : UICollectionViewDelegateFlowLayout, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  1
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: self.collectionView.frame.width, height: (self.viewModel.allIsLoad || self.viewModel.inSearchProcessing) ? 0 : 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if(!viewModel.inSearchProcessing && indexPath.item>=self.viewModel.pageSize-1 && indexPath.item == ((self.viewModel.dataSource?.data.value.count ?? 1)-1) ){
            self.viewModel.loadMoreArticles(fromFirstPage: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let dataSource = self.viewModel.dataSource{
            self.searchBar?.endEditing(true)
            let detailController = NewsDetailViewController(article: dataSource.data.value[indexPath.row])
            self.navigationController?.pushViewController(detailController, animated: true)
        }
    }
}

extension NewsViewController : UIScrollViewDelegate{
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if(velocity.y>0) {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIView.AnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                print("Hide")
            }, completion: nil)
            
        } else {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIView.AnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                print("Unhide")
            }, completion: nil)
        }
    }
}

extension NewsViewController: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel.searchWithText(searchText: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        self.viewModel.searchWithText(searchText: "")
        searchBar.endEditing(true)
        UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions(), animations: {
            [weak self] in
            self?.navigationItem.titleView = self?.titleView
            self?.navigationItem.rightBarButtonItem = self?.openSearchBarButton
        }, completion: nil)
    }
}

class LoadFooterView: UICollectionReusableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: self.frame.width/2-25, y: 0, width: 50, height: 50))
        activityIndicator.color = UIColor.gray
        activityIndicator.startAnimating()
        self.addSubview(activityIndicator)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
